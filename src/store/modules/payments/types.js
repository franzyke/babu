export const actions = {
	setCustomer: 'payments/setCustomer',
	setSale: 'payments/setSale',
	getPayments: 'payments/getPayments',
}

export const getters = {
	sales: 'payments/sales',
	salesForSelect: 'payments/salesForSelect',
	customers: 'payments/customers',
	customersForSelect: 'payments/customersForSelect',
	currentCustomer: 'payments/currentCustomer',
	currentSale: 'payments/currentSale',
	payments: 'payments/payments',
}

export const mutations = {
	FETCH_SALES: 'payments/FETCH_SALES',
	RECEIVED_SALES: 'payments/RECEIVED_SALES',
	RECEIVE_SALE: 'payments/RECEIVE_SALE',
	RECEIVE_CUSTOMER: 'payments/RECEIVE_CUSTOMER',
	FETCH_CUSTOMERS: 'payments/FETCH_CUSTOMERS',
	RECEIVED_CUSTOMERS: 'payments/RECEIVED_CUSTOMERS',
	FETCH_PAYMENTS: 'payments/FETCH_PAYMENTS',
	RECEIVE_PAYMENTS: 'payments/RECEIVE_PAYMENTS',
}