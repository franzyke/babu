// export const FETCH = 'itemss/FETCH'
// export const RECEIVED = 'items/RECEIVED'
// export const RECEIVED_ITEM = 'items/RECEIVED_ITEM'
// export const SAVE = 'items/SAVE'
// export const SAVED = 'items/SAVED'
// export const UPDATE = 'items/UPDATE'
export default{
    FETCH : 'customers/FETCH',
    RECEIVED : 'customers/RECEIVED',
    RECEIVED_CUSTOMER : 'customers/RECEIVED_CUSTOMER',
    RECEIVED_CURRENT_CUSTOMER : 'customers/RECEIVED_CURRENT_CUSTOMER',
    SAVE : 'customers/SAVE',
    SAVED : 'customers/SAVED',
    REMOVE : 'customers/REMOVE',
    REMOVED : 'customers/REMOVED',
    FETCH_HISTORY : 'customers/FETCH_HISTORY',
    RECEIVED_HISTORY : 'customers/RECEIVED_HISTORY',
}
