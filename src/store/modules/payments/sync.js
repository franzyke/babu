import * as types from './types'

export default {
    fetch_sales: (store, mutation, state, db)=>{
        let customerId = mutation.payload
        function map(doc) {
            if (doc.type === 'sales' && doc.balance > 0) {                
                emit(doc.customer._id, doc);
            }
        }
        db.query(map, {include_docs : true, key: customerId}).then(function (result) {
            store.commit(types.mutations.RECEIVED_SALES, result.rows)
        }).catch(function (err) {
            console.log(err);
        });
    },
    fetch_customers: (store, mutation, state, db)=>{
        db.allDocs({
            include_docs:true,
            startkey: 'customers_',
            endkey: 'customers_\uffff'
        })
        .then(result=>{
            var rows = result.rows
            store.commit(types.mutations.RECEIVED_CUSTOMERS, rows);
        })
    },
    fetch_payments: (store, mutation, state, db)=>{
        db.allDocs({
            include_docs: true,
            startkey: 'payments_',
            endkey: 'payments_\uffff',
            descending: true
        })
        .then(result=>{
            var rows = result.rows
            store.commit(types.mutations.RECEIVE_PAYMENTS, rows)
        })
    }
}