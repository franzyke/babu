import * as types from './types'
import actions from './actions'
import getters from './getters'
import mutations from './mutations'

const state = {
    salesForSelect: [],
    customersForSelect: [],
    sales: [],
    customers: [],
    currentCustomer: null,
    currentSale: null,
    all: []
}

export default{
	state,
	mutations,
	actions,
	getters,
    types,
}