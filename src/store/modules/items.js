import types from '../mutation-types'

const state = {
    all: [],
    currentItem: {}
}

// mutations
const mutations = {
    [types.sales.REDUCE_STOCK](state, item){
        let qq = state.all.find((s)=>{
            return s._id == item._id
        })
        
        qq.stock = qq.stock - 1;
    },
    [types.items.FETCH](state) {
	},
    [types.items.RECEIVED](state, items){
        var stateItems = []
        items.forEach(function(v){
            stateItems.push(v.doc)
        })
        state.all = stateItems
	},
    [types.items.RECEIVED_ITEM](state, item){
        state.all.push(item)
    },
    [types.items.RECEIVED_CURRENT_ITEM](state, itemId){
        state.currentItem = state.all.find((c)=>{return c._id == itemId});
	},
    [types.items.SAVE](state, item){
    },
    [types.items.EDIT](state, item){
        if(typeof item === 'string'){
            state.currentItem = state.all.find((i)=>{return i._id == item})
        }else{
            state.currentItem = item
        }
    },
    [types.items.UPDATE](state, payload){
        let item = payload.item
        let stock = payload.stock
        let price = payload.price
        let cost = payload.cost

        let i = state.all.find((itm)=>itm._id == item._id)
        i.stock = stock
        i.price = price
        i.cost = cost
    },
    [types.items.REMOVE](state, item){
    },
    [types.items.REMOVED](state, item){
        state.all.splice(state.all.indexOf(item), 1)
    },
    [types.items.RESET](state, item){
        state.currentItem = {}
    },
    [types.items.FETCH_HISTORY](state, itemId){
        // state.currentCustomer = state.all.find((c)=>{return c._id == customerId});
	},
    [types.items.RECEIVED_HISTORY](state, history){
        state.customerHistory = history;
    }
}

const actions = {
    getItems : ({ commit }) => {
        commit(types.items.FETCH)
    },
    saveItem : ({ commit }, item) => {
        commit(types.items.SAVE, item);
    },
    removeItem : ({ commit }, item) => {
        commit(types.items.REMOVE, item);
    },
    editItem : ({ commit }, item) => {
        commit(types.items.EDIT, item);
    },
    updateItem:({commit}, payload) => {
        commit(types.items.UPDATE, payload)
    },
    // setCurrentItemById : ({ commit }, itemId) => {
    //     console.log(itemId)
    //     commit(types.items.EDIT, itemId);
    // },
    resetCurrent : ({ commit }, item) => {
        commit(types.items.RESET, item);
    },
    setCurrentItemById : ({ commit }, itemId) => {
        console.log(itemId)
        commit(types.items.RECEIVED_CURRENT_ITEM, itemId);
    },
    getItemHistory : ({ commit }, itemId) => {
        commit(types.items.FETCH_HISTORY, itemId);
    },
}

const getters = {
    allItemsForSalesView: state=>{
        let items = []
        state.all.forEach(function(v){
            if(v.stock >= 1){
                items.push({
                    id:v._id,
                    text: v.name
                })
            }
        })
        return items
    },
	allItems: state=>{
        return state.all
    },
    item: (state)=>{
		return state.currentItem;
	},
    getItemHistory: (state)=>{
		return state.itemHistory;
	}
}

export default {
	state,
	mutations,
	actions,
	getters,
}
