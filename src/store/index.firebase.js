import Vue from 'vue'
import Vuex from 'vuex'

import items from './modules/items'
import customers from './modules/customers'
import sales from './modules/sales'
import firebaseSyncPlugin  from './plugins/firebase-sync'

Vue.use(Vuex)

import firebase from 'firebase'

var config = {
	apiKey: "AIzaSyBIofdJbdF12dHBiiPvsQREatHZihxisUw",
	authDomain: "babu-e4d86.firebaseapp.com",
	databaseURL: "https://babu-e4d86.firebaseio.com",
	storageBucket: "babu-e4d86.appspot.com",
	messagingSenderId: "787911452090"
};

firebase.initializeApp(config);

var db = firebase.database();
const firebaseSync = firebaseSyncPlugin(db)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
	modules: {
		items,
		customers,
		sales,
	},
	plugins: [
		firebaseSync
	],
	strict: debug,
	// plugins: debug ? [createLogger()] : []
})
