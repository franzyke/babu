export default [
	{
		path: '/',
		component: require('./components/Home.vue'),
		name: 'home'
	},
	{
		path: '/items',
		component: require('./components/items/Items.vue'),
		name: 'items.index'
	},
	{
		path: '/items/:_id',
		component: require('./components/items/Item.vue'),
		name: 'items.show' 
	},
	{
		path: '/sales',
		component: require('./components/sales/Sales.vue'),
		name: 'sales.index' 
	},
	{
		path: '/sales/:_id',
		component: require('./components/sales/Sale.vue'),
		name: 'sales.show' 
	},
	{
		path: '/sales/babu',
		component: require('./components/sales/Babu.vue'),
		name: 'sales.babu' 
	},
	{
		path: '/customers',
		component: require('./components/customers/Customers.vue'),
		name: 'customers.index' 
	},
	{
		path: '/customers/:_id',
		component: require('./components/customers/Customer.vue'),
		name: 'customers.show' 
	},
	{
		path: '/payments',
		component: require('./components/sales/Payments.vue'),
		// Needs runtime-only  + compiler included thingy
		// component: {
		// 	name: 'new-payment',
		// 	template: require('./components/payments/NewPayment.vue')
		// },
		name: 'payments.index' 
	},
];