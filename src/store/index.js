import Vue from 'vue'
import Vuex from 'vuex'

import serverSyncPlugin from './plugins/server-sync'
import items from './modules/items'
import customers from './modules/customers'
import sales from './modules/sales'
import payments from './modules/payments'

// import createLogger from '../../node_modules/vuex/src/plugins/logger'
import PouchDb from 'pouchdb'

var db = new PouchDb('babu')
// var db = new PouchDb('http://164.100.124.146:4984/traffic', {ajax: {withCredentials:false}})

// var db = new PouchDb('http://localhost:5984/traffic')

Vue.use(Vuex)

const serverSync = serverSyncPlugin(db)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
	modules: {
		items,
		customers,
		sales,
		payments,
	},
	plugins: debug ? [
		serverSync
		] : [
		serverSync,
	],
	strict: debug
})
