import * as types from './types'

export default {
	[types.getters.salesForSelect]: state=>{
		return state.salesForSelect
	},
	[types.getters.customersForSelect]: state=>{
		return state.customersForSelect
	},
	[types.getters.sales]: state=>{
		return state.sales
	},
	[types.getters.customers]: state=>{
		return state.customers
	},
	[types.getters.currentSale]: state=>{
		return state.currentSale
	},
	[types.getters.currentCustomer]: state=>{
		console.log('getter currentCustomer');
		return state.currentCustomer
	},
	[types.getters.payments]: state=>{
		return state.all
	}
}