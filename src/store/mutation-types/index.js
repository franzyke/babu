import items from './items'
import customers from './customers'
import sales from './sales'

export default{
    items,
    customers,
    sales,
}
