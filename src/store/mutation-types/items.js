// export const FETCH = 'itemss/FETCH'
// export const RECEIVED = 'items/RECEIVED'
// export const RECEIVED_ITEM = 'items/RECEIVED_ITEM'
// export const SAVE = 'items/SAVE'
// export const SAVED = 'items/SAVED'
// export const UPDATE = 'items/UPDATE'
export default{
    FETCH : 'items/FETCH',
    RECEIVED : 'items/RECEIVED',
    RECEIVED_ITEM : 'items/RECEIVED_ITEM',
    SAVE : 'items/SAVE',
    SAVED : 'items/SAVED',
    EDIT : 'items/EDIT',
    UPDATE : 'items/UPDATE',
    REMOVE : 'items/REMOVE',
    REMOVED : 'items/REMOVED',
    RESET : 'items/RESET',
    RECEIVED_HISTORY : 'items/RECEIVED_HISTORY',
    FETCH_HISTORY : 'items/FETCH_HISTORY',
    RECEIVED_CURRENT_ITEM : 'items/RECEIVED_CURRENT_ITEM',
}
