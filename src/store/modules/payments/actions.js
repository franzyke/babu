import * as types from './types'

export default{
	[types.actions.setCustomer]({commit}, id){
		commit(types.mutations.RECEIVE_CUSTOMER, id);
		commit(types.mutations.FETCH_SALES, id);
	},
	[types.actions.getCustomersWithBa]({commit}){
		// console.log('getCustomersWithBa')
		commit(types.mutations.FETCH_CUSTOMERS);
	},
	[types.actions.setSale]({commit}, id){
		commit(types.mutations.RECEIVE_SALE, id)
	},
	[types.actions.getPayments]({commit}, id){
		commit(types.mutations.FETCH_PAYMENTS, id)
	},
}