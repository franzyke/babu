import types from '../mutation-types'

const state = {
    all: [],
    currentCustomer: {},
    customerHistory: []
}

// mutations
const mutations = {
    [types.customers.SAVE](state, customer) {
	},
    [types.customers.RECEIVED_CUSTOMER](state, customer) {
        state.all.push(customer);
	},
    [types.customers.FETCH](state, customer) {
	},
    [types.customers.RECEIVED](state, items){
        var customers = [];
        items.forEach(function(v){
            customers.push(v.doc)
        })
        state.all = customers
	},
    [types.customers.REMOVE](state, items){
	},
    [types.customers.REMOVED](state, items){
        //state atangin ka la delete hleithei law
	},
    [types.customers.RECEIVED_CURRENT_CUSTOMER](state, customerId){
        state.currentCustomer = state.all.find((c)=>{return c._id == customerId});
	},
    [types.customers.FETCH_HISTORY](state, customerId){
        // state.currentCustomer = state.all.find((c)=>{return c._id == customerId});
	},
    [types.customers.RECEIVED_HISTORY](state, history){
        state.customerHistory = history;
    }
}

const actions = {
    getCustomers: ({ commit }) => {
        commit(types.customers.FETCH)
    },
    saveCustomer : ({ commit }, customer) => {
        commit(types.customers.SAVE, customer);
    },
    deleteCustomer : ({ commit }, customer) => {
        commit(types.customers.REMOVE, customer);
    },
    setCurrentCustomerById : ({ commit }, customerId) => {
        commit(types.customers.RECEIVED_CURRENT_CUSTOMER, customerId);
    },
    getCustomerHistory : ({ commit }, customerId) => {
        commit(types.customers.FETCH_HISTORY, customerId);
    },
}

const getters = {
    allCustomers: state=>{
        return state.all
    },
	allCustomersForSaleView: state=>{
        let ret = []
        state.all.forEach(function(v){
            ret.push({
                id: v._id,
                text: v.name
            })
        })
        return ret
    },
    currentCustomer: (state)=>{
		return state.currentCustomer;
	},
    customersHistory: (state)=>{
		return state.customerHistory;
	}
}

export default {
	state,
	mutations,
	actions,
	getters,
}
