import types from '../../mutation-types'

export default {
    fetch: (store, mutation, state, db)=>{
    	db.allDocs({
            include_docs:true,
            startkey: 'sales_',
            endkey: 'sales_\uffff'
        })
        .then(result=>{
        	var rows = result.rows
            store.commit(types.sales.RECEIVED, rows);
        })
    },
    save: (store, mutation, state, db)=>{
        let id = 'sales_' + (new Date()).getTime();
        let sale = mutation.payload;

        sale._id = id;
        sale.type = 'sales';
        db
        .put(sale)
        .then(function(doc){
            store.commit(types.sales.RECEIVED_SALE, sale)
        });

        db.get(sale.item._id)
            .then(function(doc){
                let s = doc.stock;
                doc.stock = s - sale.quantity;
                db.put(doc);
                // console.log(doc)
            })
            .then((doc)=>{
                let i = sale.item
                // console.log(i)
                store.commit(types.sales.REDUCE_STOCK, i)
            })

        // item._id = id;
        // item.cost = parseFloat(item.cost);
        // item.price = parseFloat(item.price);
        // item.stock = parseFloat(item.stock);
    },
    remove: (store, mutation, state, db)=>{
        let sale = mutation.payload
        db
        .get(sale._id)
        .then(function (doc) {
            store.commit(types.sales.REMOVED, sale)
            return db.remove(doc);
        })
        .then(function(){
        });
    },
    pay_current_sale:(store, mutation, state, db)=>{
        let id = 'payments_' + (new Date()).getTime();
        let sale = mutation.payload.sale
        let paid = mutation.payload.paid

        let d = new Date();

        let date = d.getFullYear().toString() + '-' + ( d.getMonth() + 1 ).toString() + '-' + d.getDate().toString()

        
        db.get(sale._id)
            .then(function(doc){

                let payment = {
                    _id: id,
                    sale: doc,
                    paid: paid,
                    date: date,
                }
                
                doc.paid = doc.paid + paid
                doc.balance = doc.balance - paid

                db.put(payment)
                return db.put(doc)
            })
    }
}
