import types from '../../mutation-types'

export default {
    fetch: (store, mutation, state, db)=>{
    	db.allDocs({
            include_docs:true,
            startkey: 'items_',
            endkey: 'items_\uffff'
        })
        .then(result=>{
        	var rows = result.rows
            store.commit(types.items.RECEIVED, rows);
        })
    },
    save: (store, mutation, state, db)=>{
        let id = 'items_' + (new Date()).getTime();
        let item = mutation.payload;

        item._id = id;
        item.type = 'items';
        item.cost = parseFloat(item.cost);
        item.price = parseFloat(item.price);
        item.stock = parseFloat(item.stock);
        db
        .put(item)
        .then(function(doc){
            store.commit(types.items.RECEIVED_ITEM, item)
        });
    },
    remove: (store, mutation, state, db)=>{
        let item = mutation.payload
        db
        .get(item._id)
        .then(function (doc) {
            store.commit(types.items.REMOVED, item)
            return db.remove(doc);
        })
        .then(function(){
        });
    },
    fetch_history: (store, mutation, state, db)=>{
        let itemId = mutation.payload
        function map(doc) {
            if (doc.type === 'sales') {
                emit(doc.item._id, doc);
            }
        }
        db.query(map, {include_docs : true, key: itemId}).then(function (result) {
            store.commit(types.customers.RECEIVED_HISTORY, result.rows)
        }).catch(function (err) {
            console.log(err);
        });
    },
    update: (store, mutation, state, db)=>{
        let item = mutation.payload.item
        let stock = mutation.payload.stock
        let price = mutation.payload.price
        let cost = mutation.payload.cost

        db.get(item._id).then(function(doc){
            doc.stock = stock
            doc.price = price
            doc.cost = cost

            return db.put(doc)
        })
    }
}
