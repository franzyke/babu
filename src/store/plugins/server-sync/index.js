import items from './items'
import customers from './customers'
import sales from './sales'

import payments from './../../modules/payments/sync'

export default (db)=> {
	return function(store){
		store.subscribe((mutation, state) => {
			if(!mutation.type){
				console.log(mutation)
				return false;
			}
			var moduleName = mutation.type.substr(0, mutation.type.indexOf('/'));
			var mutationName = mutation.type.substr((mutation.type.indexOf('/')+1), mutation.length).toLowerCase();
			switch (moduleName) {
				case 'items':
					if(typeof items[mutationName] == 'function')
						items[mutationName](store, mutation, state, db);
					break;
				case 'customers':
					if(typeof customers[mutationName] == 'function')
						customers[mutationName](store, mutation, state, db)
					break
				case 'sales':
					if(typeof sales[mutationName] == 'function')
						sales[mutationName](store, mutation, state, db)
					break
				case 'payments':
					if(typeof payments[mutationName] == 'function')
						payments[mutationName](store, mutation, state, db)
					break
				default:

			}
			// console.log(mutation)
			// console.log(state)
			// if(mutation.type == 'FETCH_SEIZURES'){
			// 	db.allDocs({include_docs:true})
			//         .then(result=>{
			//         	var rows = result.rows
			//         	// console.log(rows)
			//             store.commit('RECEIVE_SEIZURES', {rows})
			//          //    // console.log(state)
			//          //    // state.all = rows
			//          //    // console.log(state)
			//         	console.log(state);
			//          	// state.seizures.all = rows; This is a big nono
			//         })
			// }
			// if(mutation.type == 'PAY_FINE'){
			// 	db.get(mutation.payload.id)
			// 		.then(function(doc){
			// 			doc.status = 'paid';
			// 			return db.put(doc);
			// 		})
			// }
			// if(mutation.type == 'RESET_STATUS'){
			// 	db.get(mutation.payload.id)
			// 		.then(function(doc){
			// 			doc.status = 'started';
			// 			return db.put(doc);
			// 		})
			// }
		})
	}
}
