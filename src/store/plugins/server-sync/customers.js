import types from '../../mutation-types'

export default {
    fetch: (store, mutation, state, db)=>{
    	db.allDocs({
            include_docs:true,
            startkey: 'customers_',
            endkey: 'customers_\uffff'
        })
        .then(result=>{
        	var rows = result.rows
            store.commit(types.customers.RECEIVED, rows);
        })
    },
    save: (store, mutation, state, db)=>{
        console.log('asdf')
        let id = 'customers_' + (new Date()).getTime();
        let customer = mutation.payload;
        customer._id = id;
        customer.type = 'customers';
        if(!customer.purchases){
            customer.purchases = [];
        }
        db
        .put(customer)
        .then(function(doc){
            store.commit(types.customers.RECEIVED_CUSTOMER, customer)
        });
    },
    remove: (store, mutation, state, db)=>{
        let customer = mutation.payload
        db
        .get(customer._id)
        .then(function (doc) {
            store.commit(types.customers.REMOVED, customer)
            return db.remove(doc);
        });
    },
    fetch_history: (store, mutation, state, db)=>{
        let customerId = mutation.payload
        function map(doc) {
            if (doc.type === 'sales') {
                emit(doc.customer._id, doc);
            }
        }
        db.query(map, {include_docs : true, key: customerId}).then(function (result) {
            store.commit(types.customers.RECEIVED_HISTORY, result.rows)
        }).catch(function (err) {
            console.log(err);
        });
    },
}
