import types from '../mutation-types'

const state = {
    all: [],
    currentCustomer: {},
    ba:[],
    currentSale: {}
}

// mutations
const mutations = {
    [types.sales.SAVE](state, sale) {
    },
    [types.sales.RECEIVED_SALE](state, sale) {
        state.all.push(sale);
    },
    [types.sales.FETCH](state, sale) {
    },
    [types.sales.RECEIVED](state, items){
        var sales = [];
        items.forEach(function(v){
            sales.push(v.doc)
        })
        state.all = sales
    },
    [types.sales.REMOVE](state, sales){
    },
    [types.sales.REMOVED](state, sale){
        state.all.splice(state.all.indexOf(sale), 1)
        //state atangin ka la delete hleithei law
    },
    [types.sales.RESET](state){
        state.currentItem = {};
        state.currentCustomer = {}
    },
    [types.sales.SET_CURRENT_SALE](state, sale){
        state.currentSale = sale
    },
    [types.sales.PAY_CURRENT_SALE](state, payload){
        state.currentSale.balance = state.currentSale.balance - payload.paid
        state.currentSale.paid = state.currentSale.paid + payload.paid
    }
}

const actions = {
    getSales: ({ commit }) => {
        commit(types.sales.FETCH)
    },
    soldItem : ({ commit }, sale) => {
        commit(types.sales.SAVE, sale);
    },
    removeSale : ({ commit }, customer) => {
        commit(types.sales.REMOVE, customer);
    },
    resetSale : ({ commit }) => {
        commit(types.sales.RESET);
    },
    setCurrentSale : ({ commit }, sale) => {
        commit(types.sales.SET_CURRENT_SALE, sale);
    },
    payCurrentSale : ({ commit }, payload) => {
        commit(types.sales.PAY_CURRENT_SALE, payload);
    }
}

const getters = {
    allSales: (state, ba)=>{
        return state.all
    },
    allBas: state=>{
        return state.all.filter((sale)=>sale.balance > 0)
        return state.ba
    },
    currentSale: (state)=>{
       return state.currentSale;
    }
}

export default {
    state,
    mutations,
    actions,
    getters,
}
