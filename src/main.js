import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import App from './App.vue'

import store from './store'


// import '../node_modules/material-design-lite/dist/material.min.css'
// // import '../node_modules/material-design-lite/dist/material.js'
// import 'material-design-lite/material'
// import '../node_modules/material-design-icons/iconfont/material-icons.css'

Vue.use(VueRouter)

// import $ from "jquery"
// window.$ = $;

import Materials from "vue-materials"
Vue.use(Materials)

import routes from './routes'

const router = new VueRouter({
	// mode: 'history',
	routes
});

new Vue({ // eslint-disable-line no-new
	router,
	store,
	el: '#app',
	render: (h) => h(App)
})
