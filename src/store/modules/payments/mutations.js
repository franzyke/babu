import * as types from './types'

export default{
	[types.mutations.FETCH_PAYMENTS](state){
	},
	[types.mutations.RECEIVE_PAYMENTS](state, payments){
		state.all = payments
	},
	[types.mutations.FETCH_SALES](state){
	},
	[types.mutations.FETCH_CUSTOMERS](state){
	},
	[types.mutations.RECEIVED_CUSTOMERS](state, customers){
		let q = []
		customers.forEach(v=>{
			q.push({
				id: v.doc._id,
				text: v.doc.name
			})
		})
		state.customers = customers
		state.customersForSelect = q
	},
	[types.mutations.RECEIVED_SALES](state, sales){
		let q = []
		sales.forEach(v=>{
			q.push({
				id: v.doc._id,
				text: v.doc.item.name + ' - ' + v.doc.date
			})
		})
		state.sales = sales;
		state.salesForSelect = q;
	},
	[types.mutations.RECEIVE_CUSTOMER](state, customerId){
		console.log('mutations RECEIVE_CUSTOMER')
		state.currentCustomer = state.customers.find(c=>c.doc._id == customerId)
	},
	[types.mutations.RECEIVE_SALE](state, saleId){
		state.currentSale = state.sales.find(s=>s.doc._id == saleId)
	}
}