// export const FETCH = 'itemss/FETCH'
// export const RECEIVED = 'items/RECEIVED'
// export const RECEIVED_ITEM = 'items/RECEIVED_ITEM'
// export const SAVE = 'items/SAVE'
// export const SAVED = 'items/SAVED'
// export const UPDATE = 'items/UPDATE'
export default{
    FETCH : 'sales/FETCH',
    RECEIVED : 'sales/RECEIVED',
    RECEIVED_SALES : 'sales/RECEIVED_SALES',
    RECEIVED_SALE : 'sales/RECEIVED_SALE',
    SAVE : 'sales/SAVE',
    SAVED : 'sales/SAVED',
    REMOVE : 'sales/REMOVE',
    REMOVED : 'sales/REMOVED',
    REDUCE_STOCK : 'sales/REDUCE_STOCK',
    RESET : 'sales/RESET',
    BABU : 'sales/BABU',
    SET_CURRENT_SALE : 'sales/SET_CURRENT_SALE',
    PAY_CURRENT_SALE : 'sales/PAY_CURRENT_SALE',
}
